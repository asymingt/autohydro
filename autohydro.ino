#include <SPI.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetUdp.h>
#include <EEPROM.h>
#include "WebServer.h"
#include "Time.h"

// Site prefix
#define PREFIX        "/schedule"
#define PASSWORD      "password" 
#define NOIP_DOMAIN   "mydomain.ddns.net"
#define NOIP_PASSWORD "slfajs23432984329r32hffh3jfaf39r392r34732492432jr3th"
#define NOIP_EMAIL    "your@email.address"
#define TIME_ZONE     -7

// Some basic networking stuff
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; 
char t_curr[20];
char t_boot[20];

EthernetUDP Udp;
WebServer webserver(PREFIX, 80);
EthernetClient client;
unsigned int localPort = 8888;
bool foundTime = false;
bool foundDDNS = false;

// Relay definitions
#define DISABLED   0
#define HOSE1      7
#define HOSE2      6
#define HOSE3      5
#define HOSE4      4

// Schedule parameters
#define HOSE       0
#define DOW        1
#define HH         2
#define MM         3
#define DURATION   4
#define ACTIVE     5
#define SCHED_SIZE 6
#define EVENT_SIZE 16

// Update rates
unsigned long ul_PreviousMillis = 0UL;
unsigned long update_ddns_ms = 3600000UL;
unsigned long update_ntp_s = 1000;

// Watering schedule [HOSE, DOW, HH, MM, SECS, ACTIVE?]
int sched[EVENT_SIZE][SCHED_SIZE] = 
{
 {HOSE1,5,23,31,5,0},  // HOSE1: SUN 8AM 20s
 {HOSE2,5,23,31,10,0},  // HOSE1: MON 8AM 20s
 {HOSE3,5,23,31,15,0},  // HOSE1: TUE 8AM 20s
 {HOSE4,5,23,31,20,0},  // HOSE1: WED 8AM 20s 
 {HOSE1,4,8,0,20,0},  // HOSE1: THU 8AM 20s 
 {HOSE2,4,8,0,20,0},  // HOSE1: FRI 8AM 20s 
 {HOSE3,4,8,0,20,0},  // HOSE1: SAT 8AM 20s
 {HOSE4,4,8,0,20,0},  // HOSE1: SUN 8AM 20s
 {HOSE1,3,8,0,20,0},  // HOSE1: MON 8AM 20s
 {HOSE2,3,8,0,20,0},  // HOSE1: MON 8AM 20s
 {HOSE3,3,8,0,20,0},  // HOSE1: MON 8AM 20s 
 {HOSE4,3,8,0,20,0},  // HOSE1: MON 8AM 20s
 {HOSE1,2,8,0,20,0},  // HOSE1: MON 8AM 20s
 {HOSE2,2,8,0,20,0},  // HOSE1: MON 8AM 20s
 {HOSE3,2,8,0,20,0},  // HOSE1: MON 8AM 20s 
 {HOSE4,2,8,0,20,0}    // HOSE1: MON 8AM 20s
};

// If you happen to reschedule something that is curretly active, it may get into a state
// where it waters indefinitely (bad). So, when the schedule is changed, always make sure
// to reset the state of the system. TURN THE MOTORS OFF, set inactive;
void stopmotors()
{
  for (int i = 0; i < EVENT_SIZE; i++)
    sched[i][ACTIVE] = 0;
  digitalWrite(HOSE1,LOW);
  digitalWrite(HOSE2,LOW);
  digitalWrite(HOSE3,LOW);
  digitalWrite(HOSE4,LOW);
}

// handle incoming connections
void webhandle(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  // To prevent hackers from messaing with our neat system
  if (!server.checkCredentials(PASSWORD))
  {
    server.httpUnauthorized();
    return;
  }  
  
  // For post events (submitting data)
  if (type == WebServer::POST)
  {
    bool repeat;
    char token[16], value[16], needle[16];
    do
    {
      /* readPOSTparam returns false when there are no more parameters
       * to read from the input.  We pass in buffers for it to store
       * the name and value strings along with the length of those
       * buffers. */
      repeat = server.readPOSTparam(token, 16, value, 16);

      for (int i = 0; i < EVENT_SIZE; i++)
      {
        // Check the hose
        sprintf(needle,"hose%d",i);
        if (strcmp(token, needle) == 0)
           sched[i][HOSE] = atoi(value);
   
        // Check the weekday
        sprintf(needle,"dow%d",i);
        if (strcmp(token, needle) == 0)
           sched[i][DOW] = atoi(value);

        // Check the weekday
        sprintf(needle,"hh%d",i);
        if (strcmp(token, needle) == 0)
           sched[i][HH] = atoi(value);

        // Check the weekday
        sprintf(needle,"mm%d",i);
        if (strcmp(token, needle) == 0)
           sched[i][MM] = atoi(value);
           
        // Check the weekday
        sprintf(needle,"dur%d",i);
        if (strcmp(token, needle) == 0)
           sched[i][DURATION] = atoi(value);
      }
      
    } while (repeat);
    
    // Save the defaults to EEPROM and stop the motors!
    Serial.println("Saving defaults to EEPROM");
    for (int i = 0; i < sizeof(sched); i++)
      EEPROM.write(i,*((uint8_t*)sched+i));
    stopmotors();
    
    // after procesing the POST data, tell the web browser to reload
    // the page using a GET method. 
    server.httpSeeOther(PREFIX);
    return;
  }
  else if (type == WebServer::GET)
  {
    server.httpSuccess();
    server.printf(F("<html><head><title>AutoHydro Watering System</title><body><h1>Current Schedule</h1>Note that all times are PST%d.<br/><br/><form action='/schedule' method='POST'><table border='1'>"),TIME_ZONE); 
    server.print(F("<tr><td>#</td><td>Hose</td><td>Week day</td><td>HH</td><td>MM</td><td>Duration</td><td>Currently active?</td></tr>")); 
    for (int i = 0; i < EVENT_SIZE; i++)
    {
      server.printf(F("<tr><td>Event %d</td>"),i+1);      
      server.printf(F("<td><select name='hose%d'>"),i);      
      server.printf(F("<option value='%d' %s>0: Disabled</option>"),      DISABLED,(sched[i][HOSE]==DISABLED ? "selected" : ""));
      server.printf(F("<option value='%d' %s>1: Hose 1</option>"),        HOSE1,   (sched[i][HOSE]==HOSE1    ? "selected" : ""));
      server.printf(F("<option value='%d' %s>2: Hose 2</option>"),        HOSE2,   (sched[i][HOSE]==HOSE2    ? "selected" : ""));
      server.printf(F("<option value='%d' %s>3: Hose 3</option>"),        HOSE3,   (sched[i][HOSE]==HOSE3    ? "selected" : ""));
      server.printf(F("<option value='%d' %s>4: Hose 4</option>"),        HOSE4,   (sched[i][HOSE]==HOSE4    ? "selected" : ""));
      server.print(F("</select></td>"));
      server.printf(F("<td><select name='dow%d'>"),i);      
      server.printf(F("<option value='1' %s>Sunday</option>"),   (sched[i][DOW]==1 ? "selected" : ""));
      server.printf(F("<option value='2' %s>Monday</option>"),   (sched[i][DOW]==2 ? "selected" : ""));
      server.printf(F("<option value='3' %s>Tuesday</option>"),  (sched[i][DOW]==3 ? "selected" : ""));
      server.printf(F("<option value='4' %s>Wednesday</option>"),(sched[i][DOW]==4 ? "selected" : ""));
      server.printf(F("<option value='5' %s>Thursday</option>"), (sched[i][DOW]==5 ? "selected" : ""));
      server.printf(F("<option value='6' %s>Friday</option>"),   (sched[i][DOW]==6 ? "selected" : ""));
      server.printf(F("<option value='7' %s>Saturday</option>"), (sched[i][DOW]==7 ? "selected" : ""));
      server.print(F("</select></td>"));
      server.printf(F("<td><select name='hh%d'>"),i); 
      for (int j = 0; j < 24; j++)
        server.printf(F("<option value='%d' %s>%d</option>"),  j,(sched[i][HH]==j ? "selected" : ""), j);
      server.print(F("</select></td>"));
      server.printf(F("<td><select name='mm%d'>"),i); 
      for (int j = 0; j < 60; j++)
        server.printf(F("<option value='%d' %s>%d</option>"),  j,(sched[i][MM]==j ? "selected" : ""), j);      
      server.print(F("</select></td>")); 
      server.printf(F("<td><input type='text' name='dur%d' value='%d'/></td>"), i, sched[i][DURATION]);
      server.printf(F("<td>%s</td>"), (sched[i][ACTIVE] ? "yes" : "no")); 
      server.print(F("</tr>"));
    }
    server.print(F("</table><br/><input type='submit' value='Update Schedule'/></form>"));
    server.printf(F("<p>Last updated: %s</p>"),t_curr);
    server.printf(F("<p>Last reboot: %s</p>"),t_boot);
    server.print(F("</body></html>"));
  }
}

// Initial setup for the system
void setup() 
{
  // Set up the hose pins
  pinMode(HOSE1, OUTPUT);
  pinMode(HOSE2, OUTPUT);
  pinMode(HOSE3, OUTPUT);
  pinMode(HOSE4, OUTPUT);

  // Start the system
  Serial.begin(9600);
  Serial.println("AutoHydro v 2.1");
      
  // Load the defaults from EEPROM and stop the motors!
  Serial.println("Loading defaults from EEPROM");
  for (int i = 0; i < sizeof(sched); i++)
    *((uint8_t*)sched+i) = EEPROM.read(i);
  stopmotors();

  // Start the ethernet and server (we are not using DHCP because of a reset issue with the router)
  Serial.println(F("Trying to start networking"));
  if (Ethernet.begin(mac) == 0)
  {
    // Perform a software
    Serial.println("No DHCP response, so resetting board...");
    asm volatile ("  jmp 0");
  }
  Serial.print("IP number assigned by DHCP is ");
  Serial.println(Ethernet.localIP());
   
  // Start the NTP time discipline
  Udp.begin(localPort);
  setSyncProvider(getNtpTime);
  setSyncInterval(update_ntp_s);
  
  // Make sure we have at least one DDNS update
  poll_ddns();
  
  // Start the web server to modify the schedule
  webserver.setDefaultCommand(&webhandle);
  webserver.begin();

  // Save the boot time
  sprintf(t_boot,"%d-%d-%d %02d:%02d:%02d",day(),month(),year(),hour(),minute(),second());
}

time_t prevDisplay = 0; // when the digital clock was displayed

// Repeatedly called
void loop()
{  
  // Display the time if it has changed by more than a second.
  if( now() != prevDisplay)
  {
     prevDisplay = now();
     sprintf(t_curr,"%d-%d-%d %02d:%02d:%02d",day(),month(),year(),hour(),minute(),second());
     Serial.print(F("Current time: "));
     Serial.println(t_curr);
  }
    
  // STEP 1 : ACTION schedule
  poll_schedule();

  // STEP 2 : UPDATE DDNS
  poll_ddns();
  
  // STEP 3 : POLL THE WEB SERVER
  webserver.processConnection();
}


// Clock display of the time and date (Basic)
void clockDisplay()
{
  Serial.print("Current Time: ");
  Serial.print(day());
  Serial.print("-");
  Serial.print(month());
  Serial.print("-");
  Serial.print(year());
  Serial.print(" ");
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.println(" PST");
}

// Utility function for clock display: prints preceding colon and leading 0
void printDigits(int digits)
{
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SIMPLE ROUTINE TO CHECH schedule AND ACT ACCORDINGLY /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void poll_schedule()
{
  long cur_sod = 60 * (60 * (long)hour() + (long)minute()) + (long)second();
  for (int i = 0; i < EVENT_SIZE; i++)
  {
      if (sched[i][HOSE]==DISABLED) continue;   
      long lower = 60 * (60 * (long) sched[i][HH] + (long) sched[i][MM]); 
      long upper = 60 * (60 * (long) sched[i][HH] + (long) sched[i][MM]) + (long) sched[i][DURATION];
      if ((sched[i][ACTIVE]==0) && (weekday() == sched[i][DOW]) && (cur_sod >= lower) && (cur_sod <= upper))
      {
         Serial.print(F("Turning on HOSE"));
         Serial.print(sched[i][HOSE]);
         Serial.print(F(" for schedule"));
         Serial.println(i);
         digitalWrite(sched[i][HOSE],HIGH);
         sched[i][ACTIVE]=1;
      }
      if ((sched[i][ACTIVE]==1) && (weekday() == sched[i][DOW]) && (cur_sod > upper))
      {
         Serial.print(F("Turning off HOSE"));
         Serial.print(sched[i][HOSE]);
         Serial.print(F(" for schedule"));
         Serial.println(i);
         digitalWrite(sched[i][HOSE],LOW);
         sched[i][ACTIVE]=0;
      }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SIMPLE DDNS CLIENT TO SET THE DNS RECORD WITH NO-IP //////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Update the DDNS service
void poll_ddns()
{ 
  unsigned long ul_CurrentMillis = millis();
  if( ul_CurrentMillis - ul_PreviousMillis > update_ddns_ms || !foundDDNS)
  {
    ul_PreviousMillis = ul_CurrentMillis;
    do
    {
      // Update ddns
      Serial.println(F("Updating DDNS"));
      if (client.connect("dynupdate.no-ip.com", 80))
      { 
        Serial.println(F("DDNS update successful!"));
        client.println(F("GET /nic/update?hostname="));
        client.println(F(NOIP_DOMAIN));
        client.println(F(" HTTP/1.0"));
        client.println(F("Host: "));
        client.println(F(NOIP_DOMAIN));
        client.println(F("Authorization: Basic "));
        client.println(F(NOIP_PASSWORD));
        client.println(F("User-Agent: Arduino_update_client/1.0 "));
        client.println(F(NOIP_EMAIL));
        client.println();
        foundDDNS = true;
      } 
      else
      {
       Serial.println("DDNS update failed :(");         
      }
      delay(1);
      client.stop();
    }
    while (!foundDDNS);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SIMPLE NTP CLIENT TO GET THE CURRENT TIME ////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

time_t getNtpTime()
{
  IPAddress timeServer(132, 163, 4, 101);
  const int NTP_PACKET_SIZE = 48;     // NTP time is in the first 48 bytes of message
  byte packetBuffer[NTP_PACKET_SIZE]; // Buffer to hold incoming & outgoing packets
  do
  {
    while (Udp.parsePacket() > 0) ;   // Discard any previously received packets
    Serial.println(F("Transmit NTP Request"));
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    packetBuffer[0] = 0b11100011;   // LI, Version, Mode
    packetBuffer[1] = 0;            // Stratum, or type of clock
    packetBuffer[2] = 6;            // Polling Interval
    packetBuffer[3] = 0xEC;         // Peer Clock Precision
    packetBuffer[12]  = 49;
    packetBuffer[13]  = 0x4E;
    packetBuffer[14]  = 49;
    packetBuffer[15]  = 52;
    Udp.beginPacket(timeServer, 123); //NTP requests are to port 123
    Udp.write(packetBuffer, NTP_PACKET_SIZE);
    Udp.endPacket();
    uint32_t beginWait = millis();
    while (millis() - beginWait < 1500)
    {
      int size = Udp.parsePacket();
      if (size >= NTP_PACKET_SIZE)
      {
        Serial.println(F("Receive NTP Response"));
        Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
        unsigned long secsSince1900;
        // convert four bytes starting at location 40 to a long integer
        secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
        secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
        secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
        secsSince1900 |= (unsigned long)packetBuffer[43];
        foundTime = true;
        return secsSince1900 - 2208988800UL + TIME_ZONE * SECS_PER_HOUR;
      }
    }
    Serial.println(F("No NTP Response :-("));
  } while (!foundTime);
  return 0; // return 0 if unable to get the time
}
