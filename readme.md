# Autohydro #

This is an automated plant watering system written for an Arduino with the Ethernet and Relay shields. It features DHCP, an NTP-disciplined clock and DDNS support, allowing you to change the watering schedule remotely through a web-based interface. It also saves the schedule to EEPROM on change, so that the system is restored on a power cycle. 

## What do I need to build one? ##

The base system:

* 1x [Arduino Uno](https://www.arduino.cc/en/Main/ArduinoBoardUno)
* 1x [Arduino Ethenet shield](https://www.arduino.cc/en/Main/ArduinoEthernetShield)
* 1x [Arduino Relay shield v2](http://www.seeedstudio.com/wiki/Relay_Shield_V2.0)

Pumps and motors: This is really up to you. I found some small black 12V water pumps on eBay for about $6 each. I used silicon aquarium tubing to run arterial water lines from the pumps, and branched off drip valves using correctly-sized T-connectors.

## How do I get set up? ##

1. Make sure your home router has DHCP configured for internal address allocation
1. Build your Arduino system and connect it to your home router LAN
1. Sign up for a no-ip.org DDNS address
1. Checkout the autohydro code
1. Modify the preprocessor directives on lines 10 - 14 of autohydro.ino
1. Flash the code to arduino
1. Boot the arduino
1. Point your browser to http://mydomain.ddns.net/schedule
1. Put in your system password
1. Profit

## Autohydro in action ##

![11741287_10100641608523200_5876957849043694162_o.jpg](https://bitbucket.org/repo/kzLkGK/images/2745486696-11741287_10100641608523200_5876957849043694162_o.jpg)